package com.cqeec.ams.service;

import java.util.List;


import com.cqeec.ams.dao.Impl.AccountMapper;
import com.cqeec.ams.pojo.Account;
import com.cqeec.ams.service.Impl.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountMapper accountMappper;

	@Override
	public void save(Account account) {

		accountMappper.insert(account);
	}

	@Override
	public void saveAccount(Account account) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insert(Account account) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insertAccount(Account account) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insertBatch(List<Account> account) {
		// TODO Auto-generated method stub

	}

	@Override
	public Account getAccount(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void s(Account account) {
		accountMappper.insert(account);
	}

	@Override
	public void upAccount(Account account) {
		accountMappper.update(account);
	}

	@Override
	public List<Account> getAllAccount() {

		return accountMappper.findAll();
	}

	@Override
	public void AccountDelete(String id) {
		accountMappper.delete(id);
	}

}
