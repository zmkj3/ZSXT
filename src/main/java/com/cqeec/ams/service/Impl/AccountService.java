package com.cqeec.ams.service.Impl;

import com.cqeec.ams.pojo.Account;

import java.util.List;



public interface AccountService {
	public void save(Account account);

	public void saveAccount(Account account);

	public void insert(Account account);

	public void insertAccount(Account account);

	public void insertBatch(List<Account> list);

	public Account getAccount(String id);
	
	public void s(Account account);

    void upAccount(Account account);

    List<Account> getAllAccount();
	void  AccountDelete(String id);
}
