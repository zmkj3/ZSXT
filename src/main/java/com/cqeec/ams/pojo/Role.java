package com.cqeec.ams.pojo;

import java.util.Date;

/**
 * 角色。
 * 
 * @author 唐礼飞
 * @date 2019-03-14 08:39:49.056
 * @version 1.0
 */
public class Role extends AbstractEntity {
	/**
	 * 标识
	 */
	private String id;
	/**
	 * 角色名
	 */
	private String name;
	/**
	 * 禁用
	 */
	private Boolean disabled;
	/**
	 * 简介
	 */
	private String introduction;
	/**
	 * 创建时间
	 */
	private Date creationTime;

	public Role() {
	}

	public Role(String id) {
		this.id = id;
	}

	public Role(String id, String name, Boolean disabled, String introduction, Date creationTime) {
		this.id = id;
		this.name = name;
		this.disabled = disabled;
		this.introduction = introduction;
		this.creationTime = creationTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

}
