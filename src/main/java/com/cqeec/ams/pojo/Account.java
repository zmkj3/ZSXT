package com.cqeec.ams.pojo;

import java.util.Date;

/**
 * 账户。
 * 
 * @author 唐礼飞
 * @date 2019-03-12 09:30:41.664
 * @version 1.0
 */
public class Account {
	/**
	 * 标识
	 */
	private String id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 登录名
	 */
	private String loginName;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 禁用
	 */
	private Boolean disabled;
	/**
	 * 创建时间
	 */
	private Date creationTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

}
