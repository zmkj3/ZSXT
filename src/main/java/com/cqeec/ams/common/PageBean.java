package com.cqeec.ams.common;

/**
 邓学强
 */
public class PageBean {
	private int startRowNum; // 起始行号
	private int pageSize; // 每页记录数
	

	public PageBean(int startRowNum, int pageSize) {
		this.startRowNum = startRowNum;
		this.pageSize = pageSize;
	}

	public int getStartRowNum() {
		return startRowNum;
	}

	public void setStartRowNum(int startRowNum) {
		this.startRowNum = startRowNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
