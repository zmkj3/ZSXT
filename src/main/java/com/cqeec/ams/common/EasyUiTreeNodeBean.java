package com.cqeec.ams.common;
/**
 * 邓学强
 *
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class EasyUiTreeNodeBean {
	private String id;
	private String text;
	private String iconCls;
	private Boolean checked;
	private String state;
	private List<EasyUiTreeNodeBean> children;
	private Map<String, Object> attributes;

	public void addAttribute(String key, Object value) {
		if (attributes == null) {
			attributes = new HashMap<String, Object>();
		}
		attributes.put(key, value);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<EasyUiTreeNodeBean> getChildren() {
		return children;
	}

	public void setChildren(List<EasyUiTreeNodeBean> children) {
		this.children = children;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

}
