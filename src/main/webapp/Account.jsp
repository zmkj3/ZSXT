<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Basic DataGrid - jQuery EasyUI Demo</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/jeasyui/1.7.5/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/lib/jeasyui/1.7.5/themes/icon.css">

    <script type="text/javascript"
            src="${pageContext.request.contextPath}/lib/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/lib/jeasyui/1.7.5/jquery.easyui.min.js"></script>
</head>
<body>
<div id="toolBar" style="padding:10px 5px;">
    <a id="btnAdd" class="easyui-linkbutton" data-options="iconCls:'icon-add'">添加</a>
    <a id="btnEdit" class="easyui-linkbutton" data-options="iconCls:'icon-edit'">编辑</a>
    <a id="btnDelete" class="easyui-linkbutton" data-options="iconCls:'icon-remove'">删除</a>
</div>
<table  id="dg" class="easyui-datagrid" data-options="url:'${pageContext.request.contextPath}/admin/getAllAccount',fit:true,fitColumns:true,toolbar:'#toolBar',singleSelect:true">
    <thead>
    <tr>
        <th data-options="field:'name'">姓名</th>
        <th data-options="field:'loginName'">登录名</th>
        <th data-options="field:'password'">密码</th>
        <th data-options="field:'disabled'">禁用</th>
        <th data-options="field:'creationTime'">创建时间</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<div id="aeAccount" class="easyui-window" style="width:600px;height:400px"
     data-options="closed:true,modal:true,minimizable:false,collapsible:false">
    <form id="frmAccount" method="post">
        <input id="id" type="hidden" name="id" value="">
        <div style="margin:20px">
            <input class="easyui-textbox" name="name" style="width:100%" data-options="label:'姓名：',required:true">
        </div>
        <div style="margin:20px">
            <input class="easyui-textbox" name="loginName" style="width:100%" data-options="label:'昵称：',required:true">
        </div>
        <div style="margin:20px">
            <input class="easyui-passwordbox" name="password" style="width:100%" data-options="label:'密码：',required:true">
        </div>
        <div style="margin:20px">
            <input class="easyui-passwordbox" name="password2" style="width:100%" data-options="label:'确认密码：',required:true">
        </div>
        <div style="margin:20px">
            <input class="easyui-checkbox" name="disabled" value="1" label="禁用：">
        </div>
    </form>
    <div style="text-align:center;padding:5px 0">
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()" style="width:80px">保存</a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">重置</a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="closeForm()" style="width:80px">取消</a>
    </div>
</div>
<script type="application/javascript">
    $(function () {
        $("#btnAdd").click(function () {
            $("#aeAccount").window("setTitle","添加用户");
            $("#aeAccount").window("open")
        });
        $("#btnEdit").click(function () {
            $("#aeAccount").window("setTitle","编辑用户");
            $("#aeAccount").window("open");
            var row = $('#dg').datagrid('getSelected');
            getAccountByID(row);


        });
        $("#btnDelete").click(function () {
             row = $('#dg').datagrid('getSelected');
            $.ajax({

                 url:"${pageContext.request.contextPath}/admin/delete/>",

                  dataType:"json",
                 type:"post",
                success:function (data) {
                    if(data.code!=0){
                        $.messager.alert("系统消息",data.message,"info");
                    }

                  else{
                        $.messager.alert("系统消息",data.message,"info");
                    }
                }
            });
        });
    })

    function clearForm(){
        $("#frmAccount").form("clear");
    }
    function closeForm(){
        $("#aeAccount").window("close");
        $("#frmAccount").form("clear");
    }

    function getAccountByID (row) {


        $('#frmAccount').form('load',{
            id:row.id,
            name:row.name,
            loginName:row.loginName,
            password:row.password,
            disabled:row.disabled,
            creationTime:row.creationTime

        });



    }

    function submitForm()  {
        $('#frmAccount').form('submit', {
            url: "${pageContext.request.contextPath}/admin/preserve",
            onSubmit: function(){
                var isValid = $(this).form('validate');
                //两次密码验证
                pwd1=$("#frmAccount input[name='password']").val();
                pwd2=$("#frmAccount input[name='password2']").val();
                if(pwd1!=pwd2){
                    isValid=false;
                    $.messager.alert("系统提示","两次输入的密码不一致！","warning");
                    $("#frmAccount input[name='password']").focus();
                }
                if (!isValid){
                    $.messager.progress('close');
                }
                return isValid;
            },
            success: function(result){
                //result必须是JSON格式的字符串
                data=eval("("+result+")");
                $.messager.progress('close');

                if(data.code===0){
                    $.messager.alert("系统消息",data.message,"info",function(){
                        $("#aeAccount").window("close");
                    });
                }else{
                    $.messager.alert("错误消息",data.message,"error");
                }
            }
        });

    }
</script>
</body>

</html>