<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/lib/jeasyui/1.7.5/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/lib/jeasyui/1.7.5/themes/icon.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/lib/jeasyui/1.7.5/jquery.easyui.min.js"></script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false" style="height: 100px;"></div>

	<div data-options="region:'west',title:'导航菜单',split:true"
		style="width: 250px;">

		<ul id="functionTree" class="easyui-tree"
			data-options="url:'${pageContext.request.contextPath}/admin/account/permission'"></ul>

	</div>

	<div data-options="region:'center'">
		<div id="workTabs" class="easyui-tabs"
			data-options="fit:true,border:false">
			<div data-options="title:'主控制台'"></div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
			$("#functionTree")
					.tree(
							{
								onClick : function(node) {
									if (node.attributes != null
											&& node.attributes.url != null
											&& node.attributes.url != "") {
										isExists = $("#workTabs").tabs(
												"exists", node.text);
										if (isExists) {
											//功能模块已经在Tabs中打开
											$("#workTabs").tabs("select",
													node.text);
										} else {
											//Tabs没有打开功能模块
											$("#workTabs")
													.tabs(
															"add",
															{
																title : node.text,
																closable : true,
																selected : true,
																iconCls:node.iconCls,
																content : "<iframe style='width:100%;height:100%;border:none;' src='"
																		+ node.attributes.url
																		+ "'/>"
															});
										}
									}
								}
							});
		});
	</script>
</body>
</html>